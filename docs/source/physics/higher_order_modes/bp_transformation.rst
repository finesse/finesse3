.. include:: /defs.hrst

.. _transforming_beam_param:

Transformation of the beam parameter
************************************

A beam parameter is, in general, transformed when a beam passes from one medium into
another by interacting with some optical surface or propagated over any distance. We can
use the ABCD matrix formalism :cite:`siegman` to treat these transformations - giving us
the mechanics to describe how higher order modes couple between interfaces.

.. _fig_bp_transform:
.. figure:: images/transform_q.*
    :align: center

    Propagation of a beam via an interaction surface S, with a change in beam parameter
    across the different media.

Consider :numref:`fig_bp_transform`, here a beam described by the beam parameter
:math:`q_1` (propagating through a medium with refractive index :math:`n_1`) is incident
upon some interaction surface S. Through interacting with this "surface" (which could
also just be a length of free space), the beam is transformed such that it is described
by :math:`q_2` in the second medium (which has a refractive index :math:`n_2`). The
equation describing how this beam parameter is transformed through the interaction is
the Kogelnik transformation,

.. math::
        q = n_2 \frac{A \frac{q_1}{n_1} + B}{ C \frac{q1}{n_1} + D},


where :math:`A`, :math:`B`, :math:`C`, :math:`D` are the corresponding elements of the
`ABCD` matrix associated with the interaction surface S. Equations detailing the ABCD
matrix forms for the components in |Finesse| are included in the API documentation links
below. Also see :func:`finesse.gaussian.transform_beam_param` for the function which
performs the above transformation.

.. autosummary::

    finesse.components.beamsplitter.Beamsplitter.ABCD
    finesse.components.lens.Lens.ABCD
    finesse.components.mirror.Mirror.ABCD
    finesse.components.space.Space.ABCD
