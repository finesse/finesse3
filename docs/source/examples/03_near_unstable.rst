.. include:: /defs.hrst

.. _example3:

Near unstable cavities
----------------------

This example will simulate what happens as a well designed cavity is pushed close to
instability. The example show recipes for the following activities and link to more
detailed documentation:

* Designing an input telescope
* Updating model parameters
* Producing a cavity scan
* Using a CCD
* Locking a cavity

.. seealso:: :ref:`homs_usage`


The model
*********

We will consider the LIGO Pre-Mode-Cleaner design from :cite:`UeharaSPIE`. This design
is a ring resonator consisting of two flat mirrors and one curved mirror.


Designing an input telescope
****************************

In this section we will design an input telescope from a selection of standard lab
optics.

Plotting a cavity scan
**********************

In this section we will show how the cavity control signals change as the cavity is
pushed closer to instability.
