Developer guide
===============

.. toctree::
    :maxdepth: 3

    setting_up
    codeguide/index
    code_overview
    tools
    packaging
    releasing
    documenting
    extensions
    code_style
    testing/index
    ci
    debugging
