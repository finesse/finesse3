.. include:: /defs.hrst

.. _releasing:

Releasing finesse
-----------------

|Finesse| is released on two channels, `pypi <https://pypi.org/project/finesse/>`_ and
`conda-forge <https://anaconda.org/conda-forge/finesse>`_.

Before releasing, you should always make sure the :ref:`changelog<updating_changelog>`
 is up to date!

Since the conda-forge release is based on the pypi release, we will describe the PyPi
release first.

PyPi
****

The PyPi release is configured via the gitlab pipelines :source:`/.gitlab-ci.yml`, in
the ``deploy`` stage. You can trigger a release at any time by creating a tag on any
branch, but preferably we tag a commit on develop that has already passed the tests and
following `pep 440 <https://peps.python.org/pep-0440/#pre-releases>`_. Since Finesse 3
is currently in alpha stage, the version follows ``3.0aN``, where ``N`` is an increasing
integer.

The tag should be accompanied by a short description of user-facing changes since the
last release.

The ``pypy`` job currently pushes a source distribution (a compressed ``tar.gz``
archive) and a binary distributions (wheels) for `manylinux
<https://github.com/pypa/manylinux>`_ and MacOS arm64.

The source distribution is important because it is the basis for the conda-forge release
and a fallback for platforms that we do not build wheels for. Installing finesse from
PyPi on these platforms will trigger a build of the cython modules, requiring Cython and
Suitesparse to be available on the system. This is similar to a
:ref:`source-installation`.

The PyPi release is usually fairly straightforward, since binary wheels are being
created in every pipeline, catching any issues early during development. As of 3.0a23
Windows wheels are not being published.

Conda-forge
***********

The conda-forge release is in principle automated, but can be more difficult to debug
when issues arise. Much of the process is also documented `here
<https://conda-forge.org/docs/maintainer/updating_pkgs/>`_.

The `conda-forge feedstock
repository <https://github.com/conda-forge/finesse-feedstock>`_ is where the release
process is configured. To able to release finesse, your github account needs to be added
to the maintainer list. New maintainers can be added by opening an issue in the
feedstock with the title ``@conda-forge-admin, please add user @username``. This creates
a pull request which can be merged by existing maintainers.

Almost all behaviour is configured in the
`meta.yaml <https://github.com/conda-forge/finesse-feedstock/blob/main/recipe/meta.yaml>`_
which is documented
`here <https://conda-forge.org/docs/maintainer/adding_pkgs/#the-recipe-metayaml>`_.

Automatic releases
~~~~~~~~~~~~~~~~~~

It runs a bot which watches the finesse PyPi repository and automatically creates a pull
request when a new PyPi version is released. See an example
`here <https://github.com/conda-forge/finesse-feedstock/pull/34>`_. The creation of the
pull request triggers a series of builds on the conda-forge platform, building binary
conda packages for Finesse on a variety of platforms and python versions. See an example
`here <https://github.com/conda-forge/finesse-feedstock/runs/18861244994>`_. This process
is called 'rendering'.

When issues inevitably arise, you will need to fix the configuration by `committing to
the branch the bot
created <https://conda-forge.org/docs/maintainer/updating_pkgs/#pushing-to-regro-cf-autotick-bot-branch>`_,
and adding a comment in the PR with the text ``@conda-forge-admin please rerender``.
When the builds for all platforms are successful the pull request should be merged to
finalize the release.

Manual releases
~~~~~~~~~~~~~~~

If you do not want to wait for the bot to create the pull request, you can do so
yourself. You will need to fork the feedstock and make the following changes in the
``meta.yaml`` file:

- Reset the ``build->number`` to 0 (when not patching an existing release)
- Update the ``source->sha256`` to the hash of the `corresponding source release on PyPi
  <https://pypi.org/project/finesse/3.0a22/#copy-hash-modal-764eedee-1be0-49eb-90d5-d80fe6e52a10>`_
- Check if the requirements defined in the ``meta.yaml`` still match those defined in
  the :source:`pyproject.toml`
- See the current list of ``source->patches`` are functioning and whether any new
  patches are required.

Afterwards, you create a pull request of a branch in your fork into the original
feedstock repository. You can then follow the flow described above.

Patches
~~~~~~~

Releasing for conda-forge unfortunately requires some patching of the finesse
repository, mostly to the different files specifying versions. They are listed under
``source->patches`` and defined in the various ``.patch`` files in the ``recipe``
folder.

Patches failing usually results in a ``hunk fail`` in the logs of the rendering.

Updating requirements
~~~~~~~~~~~~~~~~~~~~~

The main source of problems is the different specifications for the package
requirements. The ``meta.yaml`` file can conflict with the finesse ``pyproject.toml``.
For automatic releases the bot will perform a 'Dependency Analysis' that is meant to
flag mismatches between the two specifications. See `here
<https://github.com/conda-forge/finesse-feedstock/pull/30#issue-1858034816>`_ for an
example.

The requirements are spread between ``build``, ``host`` and ``run`` sections. See `here
<https://conda-forge.org/docs/maintainer/adding_pkgs/#requirements>`_ for a brief
explanation.

Another possible source of failure is our dependencies themselves having misconfigured
their conda feedstock, leading to version conflicts.

Chat channel
~~~~~~~~~~~~

When getting stuck or confused, you can always try the asking questions in the `chat
channel <https://conda-forge.org/community/getting-in-touch/#gitter-and-element>`_
