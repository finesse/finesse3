.. include:: ../defs.hrst

.. _code_overview:

===============================
Finesse internal code structure
===============================

Interferometer Matrix Equation
==============================

.. math::
    :label: ifo_matrix

    \biggl( \begin{matrix} \text{interferometer}  \\ \text{matrix} \end{matrix} \biggl)
    \times \biggl(
        \vec{x}_{\rm sol}
    \biggl) = \biggl(
        \vec{x}_{\rm RHS}
    \biggl)

The whole of Finesse (excluding things like the parser, and beamtracing/propagation
code) revolves around setting up and solving the matrix equation :eq:`ifo_matrix` by
inverting the interferometer matrix and multiplying it with :math:`\vec{x}_{\rm RHS}`

There is a separate matrix for the carrier and :ref:`signal calculations
<signalmatrix_reference>`

Most of this page mentions code currently implementing the sparse solver, which use
:ref:`Suitesparse <https://github.com/DrTimothyAldenDavis/SuiteSparse>` KLU sparse
matrix solver under the hood. In principle the code architecture supports implementing
different solvers, like a possible graph-based symbolic solver.

Solution Vector
_______________

The desired output is the solution vector :math:`\vec{x}_{\rm sol}`. In the codebase
this vector is named ``out_view`` in :source:`simulations/homsolver.pxd`. It contains
the field (light amplitude and phase) at every :ref:`node_reference`, for every
frequency and for every higher order mode.

The index into this vector is calculated in the
:meth:`finesse.simulations.homsolver.HOMSolver.field` method in
:source:`simulations/homsolver.pyx`, which is used in the
:meth:`finesse.simulations.homsolver.HOMSolver.get_out` to get a single value and
:meth:`finesse.simulations.homsolver.HOMSolver.node_field_vector` to get a pointer to
the HOM vector.

Most of the detectors end up calling one of these functions (indirectly, via their
workspaces) to get the field at a certain node and do some further calculations. See for
instance ``c_ad_single_field_output`` in :source:`detectors/compute/amplitude.pyx`.

Right hand side vector
______________________

The right hand side vector :math:`\vec{x}_{\rm RHS}` has the same structure as the
solution vector, but all the values are initially set to zero, except for source nodes
such as lasers and modulators (described in more detail in :ref:`input_fields`).

Source values are set using the
:meth:`finesse.simulations.sparse.solver.SparseSolver.set_source` and
``set_source_fast`` functions defined in :source:`simulations/sparse/solver.pyx`. You
can see it is being indexed with the same ``field`` and ``field_fast`` functions
mentioned above.

You can see an example in ``c_laser_carrier_fill_rhs`` in
:source:`components/modal/laser.pyx`, where the laser field values are being set.

Interferometer Matrix
_____________________

The interferometer matrix contains the linear equations that couple the field values of
different nodes together.

The following simple example describes the field couplings for a mirror (also described
here :ref:`mirror_coupling`):

.. image:: ../images/line.svg
    :align: center


.. math::
    :label: mirror_coupling

    \left( \begin{array}{c} \rm Out1 \\ \rm Out2 \end{array} \right)=\left(
    \begin{array}{cc} a_{11} &a_{21}  \\ a_{12} & a_{22} \end{array}\right) \left(
    \begin{array}{c} \rm In1 \\ \rm In2 \end{array}\right)



.. math::
    :label: mirror_matrix

    \left(\begin{array}{cccc} 1 & 0 & 0 & 0 \\ -a_{11} & 1 & -a_{21} & 0 \\ 0 & 0 & 1 &
    0 \\ -a_{12} & 0 &-a_{22} & 1 \end{array}\right)\times \left(\begin{array}{c} {\rm
    In1} \\ {\rm Out1} \\ {\rm In2}\\ {\rm Out2}\\\end{array}\right)=
    \left(\begin{array}{c} {\rm In1} \\ 0 \\ {\rm In2}\\ 0\\\end{array}\right).

In a typical interferometer, most components only couple with components directly
attached to them. Therefore the interferometer matrix will be 'sparse' (most entries are
zero).

CCSMatrix
^^^^^^^^^

For memory efficiency and performance, finesse stores the IFO matrix in compressed
column storage format (CCS/CSC), which is implemented in the
:class:`finesse.cymath.cmatrix.CCSMatrix` class in :source:`cymath/cmatrix.pyx`. `This
resource
<https://matteding.github.io/2019/04/25/sparse-matrices/#CompressedSparseRow%2FColumn>`_
has a nice explanation of the format.

The most important methods are:

    - :meth:`finesse.cymath.cmatrix.CCSMatrix.declare_equations`
    - :meth:`finesse.cymath.cmatrix.CCSMatrix.declare_submatrix_view`
    - :meth:`finesse.cymath.cmatrix.CCSMatrix.construct`

Multiple calls to ``declare_equations`` define the structure and the order of the sparse
matrix. Since the sparse matrix represents a linear system of equations, every row
relates to an 'equation' coupling the corresponding field to other fields in the system
of equations, with the values in the row describing the coupling coefficients. Equation
:math:numref:`mirror_matrix` describes the following 4 equations:

.. math::

    \begin{aligned}
    & \mathrm{In1} && = \mathrm{In1} \\
    -a_{11} \cdot & \mathrm{In1} + \mathrm{Out1} - a_{21} \cdot \mathrm{In2} && = 0 \\
    & \mathrm{In2} && = \mathrm{In2} \\
    -a_{12} \cdot & \mathrm{In1} + \mathrm{Out2} - a_{22} \cdot \mathrm{In2} && = 0 \\
    \end{aligned}

We can set up this problem using :class:`finesse.cymath.cmatrix.KLUMatrix` (which
derives from :class:`finesse.cymath.cmatrix.CCSMatrix`, but interfaces to KLU as well so
we can solve the matrix at the end):

.. jupyter-execute::

    import numpy as np
    from finesse.cymath.cmatrix import KLUMatrix

    mat = KLUMatrix(name="mirror_example")
    In1 = mat.declare_equations(Neqs=1, index=0, name="I_In1", is_diagonal=True)
    Out1 = mat.declare_equations(Neqs=1, index=1, name="I_Out1", is_diagonal=True)
    In2 = mat.declare_equations(Neqs=1, index=2, name="I_In2", is_diagonal=True)
    Out2 = mat.declare_equations(Neqs=1, index=3, name="I_Out2", is_diagonal=True)

    mat.construct()
    mat.print_matrix()

Here we also see the function of :meth:`finesse.cymath.cmatrix.CCSMatrix.construct`,
which needs to be called after declaring the equations to allocate the memory and fill
in the values.

So far we have only set up a system of 4 equations where every field only directly
couples to itself (setting ``is_diagonal=True`` fills in a ``1`` in the diagonal
positions). Note that by setting ``Neqs=1`` we only declare a single equations per
index, but we can also declare a block of equations (representing higher order modes for
example).

To set up coupling between the different fields, we use
:meth:`finesse.cymath.cmatrix.CCSMatrix.declare_submatrix_view`:

.. note::

    To make the matrix printing work correctly, we have to comply with the convention
    that the names of submatrices created by
    :meth:`finesse.cymath.cmatrix.CCSMatrix.declare_equations` start with an ``I``,
    while submatrices created by
    :meth:`finesse.cymath.cmatrix.CCSMatrix.declare_submatrix_view` should not start
    with an ``I``.

.. jupyter-execute::

    a11 = mat.declare_submatrix_view(In1.from_idx, Out1.from_idx, "_In1->Out1", False)
    a22 = mat.declare_submatrix_view(In2.from_idx, Out2.from_idx, "_In2->Out2", False)

    a12 = mat.declare_submatrix_view(In1.from_idx, Out2.from_idx, "_In1->Out2", False)
    a21 = mat.declare_submatrix_view(In2.from_idx, Out1.from_idx, "_In2->Out1", False)

    mat.construct()
    mat.print_matrix()

    print(f"{a11.shape=}")

The first two arguments are the indices of the nodes between which we want to declare a
coupling. In this case, since there is only 1 equations per node, we are creating a
memory view to a 1x1 matrix, which represents a single coupling coefficient. The power
of this abstraction is more apparent when you have multiple equations per node, for
which ``declare_submatrix_view`` returns a view of a dense matrix of which the elements
are located at the appropriate locations in the overall sparse matrix.

We can now 'fill' the sparse matrix by overwriting the values in the submatrix views
using the NumPy array slicing syntax:

.. jupyter-execute::

    r = np.sqrt(0.9)
    t = np.sqrt(0.1)

    a11[:] = r
    a22[:] = r
    a12[:] = 1j * t
    a21[:] = 1j * t

    mat.print_matrix()

    # print in dense format
    np.set_printoptions(precision=1, linewidth=250)
    print(mat.to_scipy_csc().todense())

.. note::

    The submatrix views deriving from :class:`finesse.cymath.cmatrix.SubCCSView` also
    provide optimised c-functions for filling values. The ``z`` in the naming refers to
    complex values, ``a`` to a scalar and ``M`` to a matrix.

    - ``fill_za`` fills the entire submatrix with the same value ``a``
    - ``fill_zm`` copies over values from matrix ``M`` with the same shape
    - ``fill_za_zm`` fills the submatrix with the scalar-matrix product of ``a * M``

    The reason they all exist as separate functions is for optimization only.

    These submatrix view objects are being kept track of in 'connections' objects like
    :class:`finesse.components.modal.mirror.MirrorOpticalConnections`, which are being
    stored in :ref:`workspace_reference` (like
    :class:`finesse.components.modal.mirror.MirrorWorkspace`)so eventually the
    workspaces can call their fill functions to fill in couplings in the matrix. See for
    example the ``mirror_fill_optical_2_optical`` in
    :source:`components/modal/mirror.pyx`, which ends up calling the
    ``fill_negative_za_zm_2`` method of ``SubCCSView``.

We have now created a matrix identical to :math:numref:`mirror_matrix`, and we can get
to 'solving' it. This consists of three steps:

- Factoring the matrix into LU form with :meth:`finesse.cymath.cmatrix.KLUMatrix.factor`
- Inverting the matrix with :meth:`finesse.cymath.cmatrix.KLUMatrix.solve`
- Multiplying the inverse matrix with :math:`\vec{x}_{\rm RHS}` to get
  :math:`\vec{x}_{\rm sol}`. This is also done by
  :meth:`finesse.cymath.cmatrix.KLUMatrix.solve`, which returns a view of
  :math:`\vec{x}_{\rm RHS}` which has been overwritten with the solution values

We will first inject an initial value for the incoming field:

.. jupyter-execute::

    mat.set_rhs(In1.from_idx, 1.0)

And finally do the solving:

.. jupyter-execute::

    mat.print_rhs()

    mat.factor()
    mat.solve()

    mat.print_rhs()

You can see the matrix in action by passing ``debug_mode=True`` via the
``simulation_options`` dict of ``finesse.model.Model.run``

.. jupyter-execute::

    import finesse
    import numpy as np

    model = finesse.Model()

    np.set_printoptions(linewidth=200, precision=1)

    model.parse(
        """
            l l1
            s s1 l1.p1 m1.p1
            m m1 R=0.9 T=0.1
        """
    )
    model.run(simulation_options={"debug_mode": True});

Note that you can have direct access to the :class:`finesse.simulations.sparse.simulation.SparseMatrixSimulation`
instance by using the context manager :meth:`finesse.model.Model.built`, and thereby to the
:class:`finesse.simulations.sparse.KLU.KLUSolver` and eventually the :class:`finesse.cymath.cmatrix.KLUMatrix`
instance:

.. jupyter-execute::

    with model.built() as sim:
        carrier_solver = sim.carrier
        klu_mat = carrier_solver.M()
        klu_mat.print_matrix()
        klu_mat.print_rhs()

KLUMatrix
^^^^^^^^^

As seen above, the  ``KLUMatrix`` class, implements methods to factor and solve sparse
matrices by linking into `KLU
<https://github.com/DrTimothyAldenDavis/SuiteSparse/blob/stable/KLU/Doc/KLU_UserGuide.pdf>`_.
KLU is a library included in `SuiteSparse
<https://github.com/DrTimothyAldenDavis/SuiteSparse>`_ which stand for “Clark Kent” `LU
factorization <https://en.wikipedia.org/wiki/LU_decomposition>`_ algorithm (as opposed
to "SuperLU").

This is also the source of the infamous "KLU_singular error", which can easily be
replicated by passing a singular matrix:

.. jupyter-execute::
    :raises:

    from finesse.cymath.cmatrix import KLUMatrix

    mat = KLUMatrix(name="singular_example")
    In1 = mat.declare_equations(Neqs=1, index=0, name="I_In1", is_diagonal=True)
    Out1 = mat.declare_equations(Neqs=1, index=1, name="I_Out1", is_diagonal=True)
    a12 = mat.declare_submatrix_view(In1.from_idx, Out1.from_idx, "_In1->Out1", False)
    a21 = mat.declare_submatrix_view(Out1.from_idx, In1.from_idx, "_Out1->In1", False)

    mat.construct()

    a12[:] = 1
    a21[:] = 1

    mat.factor()

As you can see, at the core the matrix solving is rather straightforward. Most of the
complexity of |Finesse| arises from keeping track of all the state: the components in
the model, their changing parameters, the different frequencies and higher order modes
and the connections between the components.

.. Still add class inheritance diagram for CCSMatrix class?


Simulation
==========

The simulation class takes the :ref:`model_reference` object and 'builds' a simulation
from it. This includes the following:

- Beam Tracing functionality (this can be done without solving the matrix equation)
- Storing common simulation settings
- Checks is a signal simulation is required
- Checks which :ref:`parameter_reference` are changing and update symbolic expressions
- Determines is the simulation is modal
- Keep track of the detector workspaces

The ``BaseSimulation`` class is found in :source:`simulations/simulation.pyx` and the
child class ``SparseMatrixSimulation`` in :source:`simulations/sparse/simulation.pyx`.
The separation is such that |Finesse| might support different types of solvers (like a
graph-based solver) in the feature.

Most importantly, it builds the carrier :ref:`solver_reference` and (if necessary) the
signal :ref:`solver_reference` objects. The simulation object is saved in the
:class:`finesse.analysis.actions.base.AnalysisState` object, which in turn gets passed
to the :class:`finesse.analysis.actions.base.Action._do` method of the actions.

The actions then usually call ``state.sim.run_carrier`` or ``state.sim.run_signal``,
depending on what the action needs for it's output.

You can manually create a ``SparseMatrixSimulation`` object using the
:meth:`finesse.model.Model.built` context manager:

.. warning::

    Manually running simulations like this is only for demonstration the internals of
    |Finesse|!

.. jupyter-execute::

    import numpy as np
    import finesse


    model = finesse.Model()

    model.parse(
        """
        l l1 P=1
        s s1 l1.p1 m1.p1
        m m1 R=0.1 T=0.9
        """
    )
    with model.built() as sim:
        sim.run_carrier()
        for node, val in zip(
            sim.carrier.nodes.keys(), np.array(sim.carrier.M().rhs_view[0, :]),
            strict=True
        ):
            print(node, val)

This example is loosely based on the :class:`finesse.analysis.actions.dc.DCFields`
class.

.. Add inheritance diagram


.. _solver_reference:

Solver
======

The solver object couples the :ref:`nodes <node_reference>` from the
:ref:`model_reference` to the underlying matrix solver. In principle it would support
different kinds of matrix solvers, but currently only the ``KLUMatrix`` class is used.

The inheritance tree is ``BaseSolver->HOMSolver->SparseSolver->KLUSolver``. The classes
are implemented in :source:`simulations` and :source:`simulations/sparse`.


ModelElement
============

Detector
========

Port
====

.. _node_reference:

Node
====

.. _parameter_reference:

Parameters
==========

.. _workspace_reference:

Workspace
=========

.. _model_reference:

Model
=====

.. _signalmatrix_reference:

Signal Matrix
=============

:jupyter-download-script:`Click to download example as python script <code_overview>`

:jupyter-download-nb:`Click to download example as Jupyter notebook <code_overview>`
