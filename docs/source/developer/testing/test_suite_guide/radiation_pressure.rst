.. include:: ../../../defs.hrst

.. _radiation_pressure_testing:

Plane-wave radiation pressure effects tests
===========================================

This section contains derivations of analytical formulas for radiation pressure induced
modulation of a plane-wave laser beam, in various setups. Test cases simulating these
setups can be found in the file ``plane_wave/test_radiation_pressure.py``, in the
|Finesse| validation tests folder.


.. contents::
   :local:
   :depth: 2


Mirrors
-------

.. _mirror_single_sided:

Single-Sided Free Mass
~~~~~~~~~~~~~~~~~~~~~~

.. figure:: images/radiation_pressure/01_single_side_free_mass.*
   :align: center

   Setup for the first test case, an amplitude modulated laser with two sidebands
   reflecting off a free mass.

Analytics
#########

We start with an amplitude modulated input field, such as produced by connecting a
:class:`~finesse.components.signal.SignalGenerator` to a
:class:`~finesse.components.laser.Laser`'s ``amp`` port in |Finesse|,

.. math::
   E_\mathrm{i} = E_0 \cos(\omega_0 t)
       \left(
           1 - \frac{A}{2} \left(1 - \cos(\Omega t)\right)
       \right)
   :label: eq_amp_mod_cos

where :math:`A` is the modulation index. Writing this in the complex notation used in
|Finesse| gives

.. math::
   \begin{aligned}
       E_\mathrm{i} &= E_0 e^{i \omega_0 t}
           \left(
               1 - \frac{A}{2} \left(1 - \cos(\Omega t)\right)
           \right) \\
       &= E_0 e^{i \omega_0 t}
           \left(
               1 - \frac{A}{2}
               + \frac{A}{4}\left(e^{i \Omega t} + e^{-i \Omega t}\right)
           \right)
    \end{aligned}

We can then separate this into three fields; the carrier field :math:`a_0`, and the
upper and lower sidebands :math:`a_\pm`, such that

.. math::
   E_\mathrm{i} = a_0 + a_+ + a_-,
   :label: eq_input_field_sidebands

where

.. math::
   \begin{aligned}
       a_0 &= \left(1 - \frac{A}{2}\right) E_0 e^{i \omega_0 t} \\
       a_+ &= \frac{A}{4} E_0 e^{i (\omega_0 + \Omega) t} \\
       a_- &= \frac{A}{4} E_0 e^{i (\omega_0 - \Omega) t}.
   \end{aligned}

Next, we consider this field when incident on a free-floating mirror. For a free mass,
we have

.. math::
       F(t) = m\ddot{x}(t).

Taking the Fourier transform, we see that

.. math::
   \begin{aligned}
       \tilde{F}(\omega) &= -m \omega^2 \tilde{x}(\omega) \\
       \therefore \tilde{\chi}(\omega)
           \equiv \frac{\tilde{x}(\omega)}{\tilde{F}(\omega)}
           &= -\frac{1}{m \omega^2}.
   \end{aligned}

For a force of the form :math:`F_0 \cos(\Omega t)`, we have

.. math::
   \tilde{F}(\omega) =
       \pi F_0 (\delta(\omega + \Omega) + \delta(\omega - \Omega)),

from which we can find :math:`x(t)`:

.. math::
   \begin{aligned}
       x(t) &= \frac{1}{2 \pi}\int_{-\infty}^{+\infty}
           \tilde{\chi}(\omega) \tilde{F}(\omega) e^{i \omega t} \mathrm{d}\omega \\
       &= -\frac{\pi F_0}{2\pi} \int_{-\infty}^{+\infty}
           \frac{1}{m \omega^2} e^{i \omega t}
           (\delta(\omega + \Omega) + \delta(\omega - \Omega)) \mathrm{d}\omega \\
       &= -\frac{F_0}{m \Omega^2} \frac{e^{i \Omega t} + e^{-i \Omega t}}{2} \\
       &= -\frac{1}{m \Omega^2} F_0 \cos(\Omega t).
   \end{aligned}

A force of this form arises from the beating of the two amplitude modulation sidebands
with the carrier. The force due to radiation pressure of light interacting with a mirror
is given by

.. math::
   \begin{aligned}
       F_\mathrm{rad} &= \cos{\alpha} \frac{P_{1\mathrm{i}} + P_{1\mathrm{o}} -
       P_{2\mathrm{i}} - P_{2\mathrm{o}}}{c},
   \end{aligned}
   :label: eq_radiation_pressure

where :math:`P_{\{1,2\}\{\mathrm{i,o}\}}` is the power of the light on the negative and
positive :math:`x` sides of the mirror incoming and outgoing respectively, and
:math:`\alpha` is the angle of incidence. In this simple case, :math:`\alpha = 0`, and
there is no incoming beam from the positive :math:`x` direction. The force can then be
calculated to leading order in :math:`A` from Equation :eq:`eq_amp_mod_cos` as

.. math::
   \begin{aligned}
       F_\mathrm{rad} &= \frac{|E_\mathrm{i}| ^2 + |E_\mathrm{r}| ^2 - |E_\mathrm{t}| ^2}{c} \\
       &= \frac{(1 + R - T)}{c} |E_\mathrm{i}| ^2 \\
       &= \frac{(1 + R - T)}{c}
           \left({E_0}^2 (1 - A) + A {E_0}^2 \cos(\Omega t)\right),
   \end{aligned}

where :math:`R` is the coefficient of reflectivity of the mirror. Here we are
uninterested in the D.C. term as this will not contribute to the mirror’s oscillation,
and so have

.. math::
   \begin{aligned}
       F_0 &= \frac{(1 + R - T) A {E_0}^2}{c}, \\
       \therefore x(t) &= -\frac{(1 + R - T) A {E_0}^2}{m c \Omega^2} \cos(\Omega t).
   \end{aligned}

:numref:`fig_mirror_fields` shows how an incoming field changes upon reflection from the
mirror.

.. _fig_mirror_fields:
.. figure:: images/radiation_pressure/mirror_fields.*
   :align: center

   Fields at a mirror's surface.

The incident field will be multiplied by the reflection coefficient of the mirror,
:math:`r`. The reflected light will also be phase shifted due to propagation through
space by a total amount :math:`e^{-2i \phi}`, where

.. math::
   \begin{aligned}
       \phi &= \frac{2 \pi x(t)}{\lambda} \\
       &= -\frac{2 \pi (1 + R - T) A {E_0}^2}{\lambda m c \Omega^2} \cos(\Omega t) \\
       &= -\frac{(1 + R - T) A {E_0}^2 \omega_0}{m c^2 \Omega^2} \cos(\Omega t).
   \end{aligned}

This results in the incoming beam being phase modulated by the mirror motion,

.. math::
   E_\mathrm{r} = r E_\mathrm{i} \exp(i B \cos(\Omega t)),

where

.. math::
   B = \frac{2 (1 + R - T) A {E_0}^2 \omega_0}{m c^2 \Omega^2},

which to first order gives

.. math::
   \begin{aligned}
       E_\mathrm{r} &= r E_\mathrm{i}
           \left(
               1 + i\frac{B}{2}\left(e^{i \Omega t} + e^{-i \Omega t}\right)
           \right) \\
       &= r(b_0 + b_+ + b_-),
   \end{aligned}

where

.. math::
   \begin{aligned}
       b_0 &= E_\mathrm{i} \\
       b_+ &= i\frac{B}{2} E_\mathrm{i} e^{i \Omega t} \\
       b_- &= i\frac{B}{2} E_\mathrm{i} e^{-i \Omega t}.
   \end{aligned}

Putting Equation :eq:`eq_input_field_sidebands` into this gives

.. math::
   \begin{aligned}
       b_0 &= a_0 + a_+ + a_- \\
       b_+ &= i\frac{B}{2} E_0 e^{i (\omega_0 + \Omega) t} \\
       b_- &= i\frac{B}{2} E_0 e^{i (\omega_0 - \Omega) t},
   \end{aligned}

to leading order, where we can once again see an input field and an upper and lower
sideband. The full expression for the reflected field can then be written as

.. math::
   \begin{aligned}
       E_\mathrm{r} &= r (a_0 + a_+ + a_- + b_+ + b_-) \\
       &= r E_0 e^{i \omega_0 t}
           \left(
               1 +
               \left(\frac{A + 2i B}{4}\right)
               \left(e^{i \Omega t} + e^{-i \Omega t}\right)
           \right) \\
       &= r E_0 e^{i \omega_0 t}
           \left(
               1 +
               \left(
                   \frac{A}{4}
                   + i\frac{(1 + R - T) A {E_0}^2 \omega_0}{m c^2 \Omega^2}
               \right)
               \left(e^{i \Omega t} + e^{-i \Omega t}\right)
           \right)
    \end{aligned}

Finesse
#######

The analytics above are compared to the sidebands produced by the following |Finesse|
file:

.. code-block:: katscript

    l l1 P=3
    s s1 l1.p1 m1.p1 L=5
    m m1 R=0.5 T=0.4
    free_mass m1_sus m1.mech 0.5e-3

    ad upper s1.p1.o fsig.f
    ad lower s1.p1.o -fsig.f

    sgen sig l1.amp.i 0.1
    fsig(0.1)

    xaxis(sig.f, log, 0.1, 1e7, 400)

The results are shown in :numref:`fig_01_single_side_free_mass`.

.. _fig_01_single_side_free_mass:
.. figure:: images/radiation_pressure/01_single_side_free_mass_result.*
   :align: center

   Comparison of the upper and lower output sidebands from |Finesse| with analytics.

.. _mirror_two_sides_one_mod:

Double-Sided Free Mass, One Modulation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. figure:: images/radiation_pressure/02_two_sides_one_mod_free_mass.*
   :align: center

   Setup for the second test case, an amplitude modulated laser with two sidebands
   reflecting off a free mass causing sidebands to be generated in an unmodulated laser
   incident on the other side.

Analytics
#########

This is similar to the previous test, however there are no amplitude modulation
sidebands present in the input field. As a result, we only expect to see the output
containing the phase modulation sidebands. The second laser will not contribute to the
oscillation of the mirror, as it is a purely D.C. force. The sidebands will also be
:math:`180^\circ` out of phase, as the mirror’s motion is inverted when looking from the
other side. In addition, we assume that the mirror is opaque i.e. :math:`T = 0`. For a
given input field

.. math::
   E_\mathrm{i} = E_1 \cos(\omega_0 t),

we therefore have

.. math::
   E_\mathrm{r} = r E_1
       \left(
           1 - i\frac{(1 + R) A {E_0}^2 \omega_0}{m c^2 \Omega^2}
           \left(e^{i \Omega t} + e^{-i \Omega t}\right)
       \right).

Finesse
#######

The analytics above are compared to the sidebands produced by the following |Finesse|
file:

.. code-block:: katscript

    l l1 P=3
    s s1 l1.p1 m1.p1 L=5
    m m1 R=1 T=0
    s s2 m1.p2 l2.p1 L=5
    l l2 P=2

    free_mass m1_sus m1.mech 0.5e-3

    ad upper s2.p2.o fsig.f
    ad lower s2.p2.o -fsig.f

    sgen sig l1.amp.i 0.1
    fsig(0.1)

    xaxis(sig.f, log, 0.1, 1e7, 400)

The results are shown in :numref:`fig_02_two_sides_one_mod_free_mass`.

.. _fig_02_two_sides_one_mod_free_mass:
.. figure:: images/radiation_pressure/02_two_sides_one_mod_free_mass_result.*
   :align: center

   Comparison of the upper and lower output sidebands from |Finesse| with analytics.

.. _mirror_two_sides_two_mod:

Double-Sided Free Mass, Two Modulations
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. figure:: images/radiation_pressure/03_two_sides_two_mod_free_mass.*
   :align: center

   Setup for the third test case, two amplitude modulated lasers with two sidebands each
   incident on each side of a mirror, with some arbitrary phase difference.

Analytics
#########

As the second beam is now also amplitude modulated, it will contribute to the mirror’s
motion, so we must recalculate the radiation pressure force from Equation
:eq:`eq_radiation_pressure`. For the left and right input fields,

.. math::
   \begin{aligned}
       E_\mathrm{Left} &= E_0 \cos(\omega_0 t)
           \left(
               1 - \frac{A_0}{2} \left(1 - \cos(\Omega t)\right)
           \right) \\
       E_\mathrm{Right} &= E_1 \cos(\omega_0 t)
           \left(
               1 - \frac{A_1}{2} \left(1 - \cos(\Omega t - \psi)\right)
           \right),
   \end{aligned}

we then have

.. math::
   F_\mathrm{rad} = \frac{1 + R}{c}
       \left(
           {E_0}^2 (1 - A_0) + A_0 {E_0}^2 \cos(\Omega t)
           - {E_1}^2 (1 - A_1) - A_1 {E_1}^2 \cos(\Omega t - \psi)
       \right),

or ignoring the D.C. terms,

.. math::
   F_\mathrm{rad} = \frac{1 + R}{c}
       \left(
           A_0 {E_0}^2 \cos(\Omega t) - A_1 {E_1}^2 \cos(\Omega t - \psi)
       \right).

To write this as a single cosine, we use some trigonometry:

.. math::
   \begin{aligned}
       F_\mathrm{rad} &= \frac{1 + R}{c}
           \left[
               A_0 {E_0}^2 \cos(\Omega t)
               - A_1 {E_1}^2 (\cos(\Omega t)\cos(\psi) + \sin(\Omega t)\sin(\psi))
           \right] \\
       &= \frac{1 + R}{c}
           \left[
               (A_0 {E_0}^2 - A_1 {E_1}^2 \cos(\psi)) \cos(\Omega t)
               - A_1 {E_1}^2 \sin(\psi) \sin(\Omega t)
           \right].
   \end{aligned}

Writing the term in brackets in the form :math:`D \cos(x + \theta)`, we get

.. math::
    \begin{aligned}
       D^2 &= {(A_0 {E_0}^2 - A_1 {E_1}^2\cos(\psi))}^2
           + {(A_1 {E_1}^2\sin(\psi))}^2 \\
       D &= \sqrt{
               {A_0}^2 {E_0}^4 - 2 A_0 A_1 {E_0}^2 {E_1}^2 \cos(\psi)
               + {A_1}^2 {E_1}^4
           }
    \end{aligned}

and

.. math::
   \theta = \tan^{-1}
       \left(
           \frac{
               A_1 {E_1}^2 \sin(\psi)
           }{
               A_0 {E_0}^2 - A_1 {E_1}^2 \cos(\psi)
           }
       \right).

We can now proceed in the same way as in :ref:`mirror_single_sided`, to obtain an
expression for the reflected field on the left hand side of the mirror,

.. math::
   E_\mathrm{r} = r E_{0} e^{i \omega_0 t}
       \left[
           1 +
           \left(
               \frac{A_0}{4}
               \left(e^{i\Omega t} + e^{-i\Omega t}\right)
               + i\frac{(1 + R) D \omega_0}{m c^2 \Omega^2}
               \left(
                   e^{i (\Omega t + \theta)} + e^{-i (\Omega t + \theta)}
               \right)
           \right)
       \right].

To get the field on the right hand side of the mirror, we can apply the following
(remembering that :math:`\theta` will also be affected):

.. math::
   \begin{aligned}
       E_0 &\Longleftrightarrow E_1 \\
       A_0 &\Longleftrightarrow A_1 \\
       \psi &\Longrightarrow -\psi.
   \end{aligned}

Finesse
#######

The analytics above are compared to the sidebands produced by the following |Finesse|
file:

.. code-block:: katscript

    l l1 P=3
    s s1 l1.p1 m1.p1 L=5
    m m1 R=1 T=0
    s s2 m1.p2 l2.p1 L=5
    l l2 P=4

    free_mass m1_sus m1.mech 0.5e-3

    ad upper_left s1.p1.o fsig.f
    ad lower_left s1.p1.o -fsig.f

    ad upper_right s2.p2.o fsig.f
    ad lower_right s2.p2.o -fsig.f

    fsig(0.1)
    sgen sig1 l1.amp.i 0.1
    sgen sig2 l2.amp.i 0.1 phase=29

    xaxis(fsig.f, log, 0.1, 1e7, 400)

For comparison with the analytical results, we must also include an extra factor of
:math:`e^{\pm i\psi}` in the upper / lower sidebands respectively when looking at the
right hand side field. This is because |Finesse| measures phase relative to the first
laser in the file, which in this case is the left hand side laser. The results are shown
in :numref:`fig_03_two_sides_two_mod_free_mass_left` and
:numref:`fig_03_two_sides_two_mod_free_mass_right`.


.. _fig_03_two_sides_two_mod_free_mass_left:
.. figure:: images/radiation_pressure/03_two_sides_two_mod_free_mass_result_left.*
   :align: center

   Comparison of the upper and lower output sidebands from |Finesse| and analytics for
   the reflected field on the left of the mirror.

.. _fig_03_two_sides_two_mod_free_mass_right:
.. figure:: images/radiation_pressure/03_two_sides_two_mod_free_mass_result_right.*
   :align: center

   Comparison of the upper and lower output sidebands from |Finesse| and analytics for
   the reflected field on the right of the mirror.

Beamsplitters
-------------

The beamsplitter behaves similarly to the mirror, with two key differences. Firstly, the
angle of incidence :math:`\alpha` in Equation :eq:`eq_radiation_pressure` is no longer
necessarily 0. This also affects the phase change on reflection due to the beamsplitter
tuning, :math:`\phi`. Secondly, there are now 4 ports at which light can enter and exit,
rather than two. Here we repeat similar experiments to the previous section, taking
these factors into account.

Single-Sided Free Mass
~~~~~~~~~~~~~~~~~~~~~~

.. figure:: images/radiation_pressure/04_beamsplitter_single_side_free_mass.*
   :align: center

   Setup for the first beamsplitter test case, an amplitude modulated laser with two
   sidebands reflecting off a free mass at some non-zero angle.

Analytics
#########

The steps here are much the same as in :ref:`the equivalent mirror case
<mirror_single_sided>`, with the only changes being the inclusion of :math:`\alpha` in
the radiation pressure force calculation, and a change in the phase shift due to mirror
tuning :math:`\phi` — the total phase shift of reflected light will now be
:math:`e^{-2i\phi\cos{\alpha}}`, as the tuning :math:`\phi` is defined relative to the
surface normal.

We therefore have

.. math::

   \begin{aligned}
       x(t) &= -\frac{(1 + R - T) A {E_0}^2}{m c \Omega^2}
           \cos(\Omega t) \cos{\alpha}.\\
       \therefore \phi &= -\frac{(1 + R - T) A {E_0}^2 \omega_0}{m c^2 \Omega^2}
           \cos(\Omega t) \cos{\alpha}.\end{aligned}

This results in the incoming beam being phase modulated by the mirror motion,

.. math::

   E_\mathrm{r} = r E_\mathrm{i} \exp(i B \cos(\Omega t)),

where

.. math::

   B = \frac{2 (1 + R - T) A {E_0}^2 \omega_0}{m c^2 \Omega^2} \cos^2{\alpha},

Following the same steps as in :ref:`the mirror case <mirror_single_sided>`, the full
expression for the reflected field can then be written as

.. math::

   \begin{aligned}
       E_\mathrm{r} &= r E_0 e^{i \omega_0 t}
           \left(
               1 +
               \left(\frac{A + 2i B}{4}\right)
               \left(e^{i \Omega t} + e^{-i \Omega t}\right)
           \right) \\
       &= r E_0 e^{i \omega_0 t}
           \left(
               1 +
               \left(
                   \frac{A}{4}
                   + i\frac{(1 + R - T) A {E_0}^2 \omega_0}{m c^2 \Omega^2}
                       \cos^2{\alpha}
               \right)
               \left(e^{i \Omega t} + e^{-i \Omega t}\right)
           \right)
       \end{aligned}

Finesse
#######

The analytics above are compared to the sidebands produced by the following simple
|Finesse| file:

.. code-block:: katscript

    l l1 P=3
    s s1 l1.p1 bs1.p1 L=5
    bs bs1 R=0.5 T=0.4 alpha=37
    s s2 bs1.p2 out.p1 L=5
    nothing out

    free_mass bs1_sus bs1.mech 0.5e-3

    ad upper s2.p2.o fsig.f
    ad lower s2.p2.o -fsig.f

    fsig(0.1)
    sgen sig l1.amp.i 0.1

    xaxis(fsig.f, log, 0.1, 1e7, 400)

The results are shown in :numref:`fig_04_beamsplitter_single_side_free_mass`.

.. _fig_04_beamsplitter_single_side_free_mass:
.. figure:: images/radiation_pressure/04_beamsplitter_single_side_free_mass_result.*
   :align: center

   Comparison of the upper and lower output sidebands from |Finesse| with analytics.

Double-Sided Free Mass, One Modulation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. figure:: images/radiation_pressure/05_beamsplitter_two_sides_one_mod_free_mass.*
   :align: center

   Setup for the second beamsplitter test case, an amplitude modulated laser with two
   sidebands reflecting off a free mass causing sidebands to be generated in an
   unmodulated laser incident on the other side.

Analytics
#########

This is similar to :ref:`the equivalent mirror case <mirror_two_sides_one_mod>`, with
the inclusion of the :math:`\cos^2{\alpha}` term from the previous section. For a given
input field

.. math::

   E_\mathrm{i} = E_1 \cos(\omega_0 t),

we therefore have

.. math::

   E_\mathrm{r} = r E_1
       \left(
           1 - i\frac{(1 + R) A {E_0}^2 \omega_0}{m c^2 \Omega^2}
           \cos^2{\alpha} \left(e^{i \Omega t} + e^{-i \Omega t}\right)
       \right).

Finesse
#######

The analytics above are compared to the sidebands produced by the following |Finesse|
file:

.. code-block:: katscript

    l l1 P=3
    s s1 l1.p1 bs1.p1 L=5

    l l2 P=2
    s s2 l2.p1 bs1.p4 L=5

    bs bs1 R=1 T=0 alpha=37
    s sout bs1.p3 out.p1 L=5
    nothing out

    free_mass bs1_sus bs1.mech 0.5e-3

    ad upper sout.p2.o fsig.f
    ad lower sout.p2.o -fsig.f

    fsig(0.1)
    sgen sig l1.amp.i 0.1

    xaxis(fsig.f, log, 0.1, 1e7, 400)

The results are shown in :numref:`fig_05_beamsplitter_two_sides_one_mod_free_mass`

.. _fig_05_beamsplitter_two_sides_one_mod_free_mass:
.. figure:: images/radiation_pressure/05_beamsplitter_two_sides_one_mod_free_mass_result.*
   :align: center

   Comparison of the upper and lower output sidebands from |Finesse| with analytics.

Double-Sided Free Mass, Two Modulations
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. figure:: images/radiation_pressure/06_beamsplitter_two_sides_two_mod_free_mass.*
   :align: center

   Setup for the third beamplitter test case, two amplitude modulated lasers with two
   sidebands each incident on each side of a beamsplitter, with some arbitrary phase
   difference.

Analytics
#########

Once again, this is similar to the procedure in :ref:`the equivalent mirror case
<mirror_two_sides_two_mod>`, with the addition of a :math:`\cos^2{\alpha}` term on the
amplitude of the generated sidebands. The output at the top detector will therefore be

.. math::

   \begin{aligned}
       E_\mathrm{r} &= r E_{0} e^{i \omega_0 t}
           \left[
               1 +
               \left(
                   \frac{A_0}{4}
                   \left(e^{i\Omega t} + e^{-i\Omega t}\right)
                   + i\frac{(1 + R) D \omega_0}{m c^2 \Omega^2}\cos^2{\alpha}
                   \left(
                       e^{i (\Omega t + \theta)} + e^{-i (\Omega t + \theta)}
                   \right)
               \right)
           \right],
   \end{aligned}

with :math:`D` defined as in :ref:`the mirror case <mirror_two_sides_two_mod>`. The
results for the right detector can be obtained with the same transformations as before.

Finesse
#######

The analytics above were compared to the sidebands produced by the following |Finesse|
file:

.. code-block:: katscript

    l l1 P=3
    s s1 l1.p1 bs1.p1 L=5

    l l2 P=4
    s s2 l2.p1 bs1.p4 L=5

    bs bs1 R=1 T=0 alpha=37

    s sout_left bs1.p2 out_left.p1 L=5
    nothing out_left

    s sout_right bs1.p3 out_right.p1 L=5
    nothing out_right

    free_mass bs1_sus bs1.mech 0.5e-3

    ad upper_left sout_left.p2.o fsig.f
    ad lower_left sout_left.p2.o -fsig.f

    ad upper_right sout_right.p2.o fsig.f
    ad lower_right sout_right.p2.o -fsig.f

    fsig(0.1)
    sgen sig1 l1.amp.i 0.1
    sgen sig2 l2.amp.i 0.1 phase=29

    xaxis(fsig.f, log, 0.1, 1e7, 400)

As in :ref:`the mirror case <mirror_two_sides_two_mod>`, for comparison with the
analytical results, we must also include an extra factor of :math:`e^{\pm i\psi}` in the
upper / lower sidebands respectively when looking at the right hand side field. This is
because |Finesse| measures phase relative to the first laser in the file, which in this
case is the left hand side laser. The results are shown in
:numref:`fig_06_beamsplitter_two_sides_two_mod_free_mass_left` &
:numref:`fig_06_beamsplitter_two_sides_two_mod_free_mass_right`.


.. _fig_06_beamsplitter_two_sides_two_mod_free_mass_left:
.. figure:: images/radiation_pressure/06_beamsplitter_two_sides_two_mod_free_mass_result_left.*
   :align: center

   Comparison of the upper and lower output sidebands from |Finesse| and analytics for
   the reflected field on the left of the beamsplitter.

.. _fig_06_beamsplitter_two_sides_two_mod_free_mass_right:
.. figure:: images/radiation_pressure/06_beamsplitter_two_sides_two_mod_free_mass_result_right.*
   :align: center

   Comparison of the upper and lower output sidebands from |Finesse| and analytics for
   the reflected field on the right of the beamsplitter.
