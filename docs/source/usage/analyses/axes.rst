.. include:: /defs.hrst

====
Axes
====

.. kat:analysis:: noxaxis

.. kat:analysis:: xaxis

    :See Also:

        :kat:analysis:`x2axis`, :kat:analysis:`x3axis`, :kat:analysis:`sweep`

.. kat:analysis:: x2axis

    :See Also:

        :kat:analysis:`xaxis`, :kat:analysis:`x3axis`, :kat:analysis:`sweep`

.. kat:analysis:: x3axis

    :See Also:

        :kat:analysis:`xaxis`, :kat:analysis:`x2axis`, :kat:analysis:`sweep`

.. kat:analysis:: sweep

    :See Also:

        :kat:analysis:`xaxis`, :kat:analysis:`x2axis`, :kat:analysis:`x3axis`

.. kat:analysis:: freqresp

    :See Also:

        :kat:analysis:`freqresp2`, :kat:analysis:`freqresp3`, :kat:analysis:`freqresp4`

.. kat:analysis:: freqresp2

    :See Also:

        :kat:analysis:`freqresp`, :kat:analysis:`freqresp3`, :kat:analysis:`freqresp4`

.. kat:analysis:: freqresp3

    :See Also:

        :kat:analysis:`freqresp`, :kat:analysis:`freqresp2`, :kat:analysis:`freqresp4`

.. kat:analysis:: freqresp4

    :See Also:

        :kat:analysis:`freqresp`, :kat:analysis:`freqresp2`, :kat:analysis:`freqresp3`

.. kat:analysis:: opt_rf_readout_phase

.. kat:analysis:: sensing_matrix_dc

.. kat:analysis:: set_lock_gains

    :See Also:

	:kat:analysis:`opt_rf_readout_phase`, :kat:analysis:`sensing_matrix_dc`

.. kat:analysis:: get_error_signals

.. kat:analysis:: update_maps
