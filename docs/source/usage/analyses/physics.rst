.. include:: /defs.hrst

=======
Physics
=======

.. kat:analysis:: eigenmodes

.. kat:analysis:: operator

.. kat:analysis:: abcd

.. kat:analysis:: beam_trace

.. kat:analysis:: propagate_beam

.. kat:analysis:: propagate_beam_astig

.. kat:analysis:: antisqueezing
