.. include:: /defs.hrst

.. _degree_of_freedom:

========================
Using degrees of freedom
========================

.. kat:element:: degree_of_freedom

The :kat:command:`degree_of_freedom` command can be useful when building control loops, but also
for tuning interferometers and any other situation where you want to change multiple
parameters of the model at the same time.

The typical degrees of freedom in an interferometer are :cite:`PhDMaggiore`:

    - DARM: Differential ARM length. The output channel of the detector. Generally
      operated with a small DC offset relative to the dark fringe
      conditions. :math:`L_N - L_W`
    - CARM: Common ARM length. Used to keep the arms on resonance. :math:`\frac{L_N + L_W}{2}`
    - MICH: Controls the short arms of the Michelson and determines the fringe at the
      dark port. :math:`l_N - l_W`

.. figure:: /images/dof_maggiore.*

    Simplified layout of AdV+

Creating a basic version of this interferometer.

.. jupyter-execute::

    import finesse
    import matplotlib.pyplot as plt
    from finesse.analysis.actions import Xaxis, Minimize, Noxaxis

    finesse.init_plotting()

    base_model = finesse.Model()
    base_model.parse(
        """
        laser l1 P=1
        bs bs1 R=0.5 T=0.5

        m WI R=0.9 T=0.1
        m NI R=0.9 T=0.1

        m WE R=1 T=0
        m NE R=1 T=0

        link(l1, bs1)
        link(bs1.p2, 1, WI.p1)
        link(bs1.p3, 1, NI.p1)

        link(WI.p2, 1, WE.p1)
        link(NI.p2, 1, NE.p1)

        pd out bs1.p4.o
        """
    )

You can inspect the available degrees of freedom for a model component by accessing
their ``.dofs`` namespace:

.. jupyter-execute::

   base_model.WE.dofs

Every degree of freedom is coupled to a parameter of the component and has DC and AC
attributes. For tuning the interferometer we are interested in the ``z.DC`` attribute.

.. jupyter-execute::

    base_model.WE.dofs.z.DC

We can see it is directly coupled to the ``phi`` attribute of the mirror. Now we create
a copy of this model with the standard degrees of freedom defined.

.. jupyter-execute::

    dof_model = base_model.deepcopy()
    dof_model.parse(
        """
        dof DARM WE.dofs.z -1 NE.dofs.z +1
        dof CARM WE.dofs.z +1 NE.dofs.z +1
        dof MICH WI.dofs.z -1 NI.dofs.z +1
        """
    )

We can reference the ``MICH`` degree of freedom to tune the dark port of the
interferometer.

.. jupyter-execute::

    from finesse.analysis.actions import Minimize

    out = dof_model.run(
        Minimize(dof_model.out, dof_model.MICH.DC, xatol=1e-15, fatol=1e-15)
    )
    dof_model.run(Noxaxis())['out']

Inspecting the mirror tunings, we can see that they are :ref:`referencing <_symbolics>`
the ``MICH.DC`` degree of freedom.

.. jupyter-execute::

    display(dof_model.WI.dofs.z.DC)
    print(f"{dof_model.WI.dofs.z.DC.eval()=} degrees")
    display(dof_model.NI.dofs.z.DC)
    print(f"{dof_model.NI.dofs.z.DC.eval()=} degrees")
    display(dof_model.MICH.DC)

Note that in the base model without the degrees of freedom defined, you can still change
the mirror tuning by referencing the ``phi`` attribute directly.

.. jupyter-execute::

    sol_phi = base_model.run(
        Xaxis(base_model.WE.phi, "lin", -180, 180, 200, relative=False)
    )
    sol_phi.plot()

But in the model with the degrees of freedom defined, you can no longer control the
mirror tuning directly.

.. jupyter-execute::
    :raises: ExternallyControlledException

    dof_model.run(Xaxis(dof_model.WE.phi, "lin", -180, 180, 200, relative=False))

It is still possible to move individual mirrors by defining additional degrees of
freedom.

.. jupyter-execute::

    dof_model.parse(
        """
        dof WE_phi WE.dofs.z +1
        """
    )
    # reset the degree of freedom so both models are in identical state
    dof_model.MICH.DC = 0
    sol_WEDC = dof_model.run(
        Xaxis(dof_model.WE_phi.DC, "lin", -180, 180, 200, relative=False)
    )

    plt.plot(sol_phi.x1, sol_phi["out"], label="WE.phi")
    plt.plot(sol_WEDC.x1, sol_WEDC["out"], label="WE_phi.DC", ls="--")
    plt.xlabel("WE position")
    plt.ylabel("Power")
    plt.title("Comparison of scanning with `.phi` and with the dof")
    plt.show()

When using degrees of freedom, it is safest to never access the component attributes
directly. The recommended way to adjust attributes is using actions, which will warn you
if you are modifying an attribute controlled by a degree of freedom.

If you want to manually adjust an attribute, using a degree of freedom is recommended
because it maintains previously defined symbolic relationships, while manually adjusting
the component attribute directly will break it.

.. jupyter-execute::

    manual_model = dof_model.deepcopy()

    display(manual_model.WE_phi.DC)
    display(manual_model.WE.phi)

    out = manual_model.run(Xaxis(manual_model.WE_phi.DC, 'lin', -180, 180, 200))
    out.plot()

    print("Adjusting degree of freedom DC parameter")
    manual_model.WE_phi.DC = 45

    display(manual_model.WE_phi.DC)
    display(manual_model.WE.phi)
    display(f"{manual_model.WE.phi.eval()=}")

    print("Adjusting mirror tuning directly")
    manual_model.WE.phi = 90

    display(manual_model.WE_phi.DC)
    display(manual_model.WE.phi)

    out = manual_model.run(Xaxis(manual_model.WE_phi.DC, 'lin', -180, 180, 200))
    out.plot()

    manual_model.WE_phi.DC = 45

    display(manual_model.WE_phi.DC)
    display(manual_model.WE.phi)
