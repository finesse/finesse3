.. include:: /defs.hrst

Linear time invariant system modelling
**************************************

One of the primary tasks undertaken with |Finesse| 2 was simulating the frequency domain
behaviour of complex optical systems. This involved computing various transfer functions
of small signals injected into the system and computing how they affect various outputs.
With knowledge of how these small signals behave in an optical system we can then model
how classical and quantum noise propagates throughout an optical system and the
behaviour of control systems designed to keep the an optical system operating.

Modelling LTI systems is a broad subject and covered in significant and better detail
else where. As background reading primarily targeted at modelling LTI systems for
sensing and control of interferometers we recommend Chapter 8 `Interferometric length
sensing and control` in :cite:`LivingReview` --- much of this section of the manual will
assume physics knowledge from this chapter.

.. note::

    Those with previous experience in modelling LTI responses in |Finesse| 2 will be
    familiar with the :kat:command:`fsig` command that was used to inject a signals.
    |Finesse| 3 still keeps the same name :kat:command:`fsig` but this is now purely the
    frequency of the small signal excitation ``Model.fsig.f`` which globally sets what
    signal frequency is used. Injecting signals is now done using signal generator
    components or special analyses like :kat:analysis:`frequency_response`.

|Finesse| 3 introduces a significantly changed interface from |Finesse| 2 for modelling
transfer functions. In this section we will cover these modelling tasks from the basic
to more advanced features.

.. toctree::
    :maxdepth: 2

    signals
    frequency_response
    loops
    degree_of_freedom
