Beamsplitter
~~~~~~~~~~~~

.. kat:element:: beamsplitter

    Phase relationship
    ------------------
    See :ref:`Mirror and beamsplitter phase relationships <beamsplitter_phase>`
    for more details on the phase relationship to ensure energy conservation
    that is used in |Finesse|.

    Ports
    -------
    The order of the ports is shown below.
    In a typical Michelson interferometer
    the order of the ports is:
    input-reflection-transmission-output.

    .. figure:: ../../../../resources/beamsplitter.svg
        :align: center

    Parameters
    ----------

    Listed below are the parameters of the beamsplitter component. Certain parameters
    can be changed during a simulation and some cannot, which is highlighted in the `can
    change during simulation` column. These changeable parameters can be used by actions
    such as ``xaxis`` or ``change``. Those that cannot must be changed before a
    simulation is run.

    .. jupyter-execute::
        :hide-code:

        from finesse.utilities.misc import doc_element_parameter_table
        from finesse.components import Beamsplitter
        doc_element_parameter_table(Beamsplitter)
