.. include:: /defs.hrst

======
Optics
======

Common optical building blocks are provided for building up an optical system. Some have
more detailed pages which are listed in the table of contents below.

.. toctree::
    :maxdepth: 2

    optics/mirror.rst
    optics/beamsplitter.rst

Directional Beamsplitter
************************

.. kat:element:: directional_beamsplitter


Isolator
********
.. kat:element:: isolator

    Parameters
    ----------

    .. jupyter-execute::
        :hide-code:

        from finesse.utilities.misc import doc_element_parameter_table
        from finesse.components import Isolator
        doc_element_parameter_table(Isolator)

Lens
****
.. kat:element:: lens

    Parameters
    ----------

    .. jupyter-execute::
        :hide-code:

        from finesse.utilities.misc import doc_element_parameter_table
        from finesse.components import Lens
        doc_element_parameter_table(Lens)


AstigmaticLens
**************
.. kat:element:: alens

    Parameters
    ----------

    .. jupyter-execute::
        :hide-code:

        from finesse.utilities.misc import doc_element_parameter_table
        from finesse.components import AstigmaticLens
        doc_element_parameter_table(AstigmaticLens)


Modulator
*********
.. kat:element:: modulator

    Parameters
    ----------

    .. jupyter-execute::
        :hide-code:

        from finesse.utilities.misc import doc_element_parameter_table
        from finesse.components import Modulator
        doc_element_parameter_table(Modulator)

Optical Bandpass
****************
.. kat:element:: optical_bandpass

    Parameters
    ----------

    .. jupyter-execute::
        :hide-code:

        from finesse.utilities.misc import doc_element_parameter_table
        from finesse.components import OpticalBandpassFilter
        doc_element_parameter_table(OpticalBandpassFilter)

Frequency Loss
**************
.. kat:element:: floss

    Parameters
    ----------

    .. jupyter-execute::
        :hide-code:

        from finesse.utilities.misc import doc_element_parameter_table
        from finesse.components import FrequencyLoss
        doc_element_parameter_table(FrequencyLoss)
