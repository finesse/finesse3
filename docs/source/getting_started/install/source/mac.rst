.. include:: ../../../defs.hrst
.. _osx-source-installation:

macOS
=====

Finesse 3 should build on both Intel and Apple (ARM) machines.

When installing on macOS you'll need to ensure you have xcode and its command line
utilities installed. Xcode can be installed from the app store. Once that is installed
open a terminal and install the command line utilities:

.. code-block:: console

    $ xcode-select --install

Use `MacPorts <https://www.macports.org/>`__ or `Brew <https://brew.sh/>`__ to install
the `suitesparse <https://people.engr.tamu.edu/davis/suitesparse.html>`__ library.
Alternatively you can also get it using `Conda
<https://anaconda.org/anaconda/suitesparse>`__ if you are using that anyway for
environment or package managing.

Getting source code
~~~~~~~~~~~~~~~~~~~

First download the source code.
Once the relevant system packages are installed simply clone the repository and build
|Finesse| as described below. By default this will install the latest stable version.


.. code-block:: console

    $ git clone -b master https://gitlab.com/ifosim/finesse/finesse3.git
    $ cd finesse3

Conda
~~~~~
If you are using Conda as your package manager run:

.. code-block:: console

    $ make develop-conda

This will install the development and build dependencies.
Installing |Finesse| can take some time as it needs to compile some extensions
before it can run. You should see
no errors and the version number installed should be printed. Once you've run this
command, you can quickly test if it has worked by running:

.. code-block:: console

    $ kat3 --version

If you see the |Finesse| version printed, everything should be configured correctly.

Macports or Homebrew
~~~~~~~~~~~~~~~~~~~~

Using either you'll need to install suitesparse and the openmp library.
Once the packages have been installed run:

.. code-block:: console

    $ make develop-pep517


Known problems
~~~~~~~~~~~~~~

macOS Catalina can sometimes thrown an error that it cannot find ``stdio.h``. This
appears to be a path setup issue. It can be fixed using

.. code-block:: console

    $ export CPATH='/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include:/opt/local/include'
    $ export LIBRARY_PATH='/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/lib:/opt/local/lib'

Rebuilding |Finesse|
--------------------

Once you have installed |Finesse| from source you can easily get updates and rebuild it
by executing the following commands from the |Finesse| repository directory:

.. code-block:: console

    $ git pull
    $ make clean
    # If you are using macports or homebrew use
    $ make develop-pep517
    # or if you are using Conda
    $ make develop-conda

This should rebuild |Finesse| and can take some time. For small changes to the source
you can omit ``make clean``.

Occasionally changes to the build tooling will require a complete reset of the local
clone of the |Finesse| repository. To do this, the command ``make realclean`` can be
used above instead of ``make clean``.

Changing |Finesse| versions after installation
----------------------------------------------

Switching versions in |Finesse| involves checking out a different git branch. The
process is similar to above when rebuilding, except you use ``git checkout`` beforehand.
It is recommended to use ``make clean`` when switching branches or commits.

.. code-block:: console

    $ cd finesse3
    $ git checkout [ref]
    $ git pull
    $ make clean
    $ make install-pep517

You can replace ``[ref]`` with one of:

``master``
    The latest stable release.

``develop``
    The latest development release (not necesarily stable or runnable).

e.g. ``3.0a1``
    A particular tag.
