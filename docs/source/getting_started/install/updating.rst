.. include:: ../../defs.hrst

.. _updating_finesse:

Updating Finesse
----------------

Since |Finesse| is still in alpha stage, we recommended staying up to date with the
latest release, since we are continuously improving the software and fixing bugs. See
the :ref:`changelog` for the latest changes and subscribe to the `matrix channel
<https://matrix.to/#/#finesse:matrix.org>`_ to get notified of new releases

Conda
~~~~~

Make sure you have your |Finesse| environment actiavted:

.. code-block:: console

    $ conda activate finesse

Update the |Finesse| package from conda-forge:

.. code-block:: console

    (finesse) $ conda update -c conda-forge finesse

Sometimes conda refuses to update to the latest version, because of version
incompatibilities. The latest version of |Finesse| is "|release|" and you can request
it:

.. code-block:: console

    (finesse) $ conda update -c conda-forge finesse==<latest_version>

Conda environment can be broken for various reasons, and refuse to install the latest
version of |Finesse|. When that happens, it is recommended to create a fresh environment
to install the latest version, following :ref:`the instructions here
<installation-conda>`. To delete an existing conda environment:

.. code-block:: console

    (finesse) $ conda activate base
    (base) $ conda env remove -n finesse

.. warning::

    Deleting your finesse environment will also delete any extra dependencies that you
    may have installed over time. You can run ``conda env export -f
    old_environment.yml`` to create a backup environment file to revert to with ``conda
    env create -f old_environment.yml``


From PyPI (with e.g. pip)
~~~~~~~~~~~~~~~~~~~~~~~~~

Simply run:

.. code-block::

    pip install -U finesse

in your virtual finesse environment. See :ref:`installation from PyPI
<installation-pypi>` for more details on setting up a virtual environment.

Source install
~~~~~~~~~~~~~~

Check out the latest version tag "|release|" (or possibly a branch or a special commit)
that you want to update to with git:

.. code-block::

    git checkout <finesse_version>

Then choose the appropriate make target for your setup:

.. code-block::

    # For a python virtual environment setup
    make develop-pep517
    # For a conda setup
    make develop-conda

See :ref:`rebuilding_extensions` for more details.
