.. include:: ../defs.hrst
.. _getting_help:

Getting help
============

For general or technical questions on using Finesse we have a user chat channel
on `Matrix <https://matrix.to/#/#finesse:matrix.org>`_.

This is open to the general public and is the easiest way to get help.

There is also a mailing list which can also be used for those that prefer email
over Matrix, finesse-users AT nikhef.nl.

For users within the gravitational wave community we also have an `active chat
channel https://chat.ligo.org/ligo/channels/finesse`. This will require you to
log in with your Albert.Einstein credentials.

.. _reporting_issues:

Reporting issues
================

You can automatically generate a helpful bug report in markdown format by calling
:func:`.finesse.utilities.bug_report.bug_report`. This will include some information
about your system and python installation, and optionally includes the code
from the file you are calling it in and the last exception that was raised.

If you want to use it in a script, you should catch the exception to report it:

.. jupyter-execute::

    import finesse

    try:
        1 / 0
    except Exception:
        report = finesse.bug_report(file="bug_report.md")
        print(report)

In an interactive notebook environment, you can call the function from a separate cell
to grab the last exception and that will also work.
It will return the report as a string and safe it to ``bug_report.md``.

.. warning::

    You can optionally include the source code of the file causing the exception in your
    report by passing ``include_source=True`` to
    :func:`.finesse.utilities.bug_report.bug_report` , but you might unintentionally
    leak proprietary/confidential information by posting the report in a public space.

You can copy the contents directly into the `Matrix
<https://matrix.to/#/#finesse:matrix.org>`_ channel or `create a gitlab issue
<https://gitlab.com/ifosim/finesse/finesse3/-/issues/new>`_

Finesse and KatScript objects
-----------------------------

Refer to the rest of this documentation for information on the use of KatScript and
Finesse objects.

Interactive help
****************

The :func:`finesse.help` function accepts either a string or object as input and will
show corresponding help. If a string is provided, it is assumed to be a KatScript
:ref:`path <syntax_paths>` like ``m``, ``beamsplitter``, or ``bs.T``. Command names such
as ``xaxis`` are also supported.

This operates similarly to the Python built-in function :func:`help`: either a pager is
opened (if using a console; use :kbd:`PgUp` and :kbd:`PgDn` to navigate, and
:kbd:`q` to escape) or the help text is printed (if using a notebook).
