.. include:: /defs.hrst
.. _contributors:

About us
========

|Finesse| has been developed with the help of many people over more than 20 years.
Thanks to everyone who has improved |Finesse| by contributing code, bug reports,
documentation, training and support for students and input on the design, features, and
the future development of the software.

In late 2017 Daniel Brown and Andreas Freise started |Finesse| 3, a re-implementation of
|Finesse| in Python, with the idea to provide a modern and clean code base that makes
further developing and extending the software simpler, especially for external
contributors. The aim is to merge the established features and reliability of |Finesse|
with a modular and hackable object based design, and to add some cool new features in
the process!

This process continued with a growing number of key contributors and we expect to
attract many more along the way:

Core Finesse development:
^^^^^^^^^^^^^^^^^^^^^^^^^

.. only:: html

  .. cssclass:: avatar
  .. figure:: /images/andreas.jpg

.. cssclass:: avatar-text
**Andreas Freise**, VU Amsterdam and Nikhef: Project lead


.. only:: html

  .. cssclass:: avatar
  .. figure:: /images/daniel.jpg

.. cssclass:: avatar-text
**Daniel Brown**, University of Adelaide: Project and programming lead


.. only:: html

  .. cssclass:: avatar
  .. figure:: /images/miron.jpeg

.. cssclass:: avatar-text
**Miron Van Der Kolk**, Nikhef: Core developer. Development of polarisation
features as well as maintenance and improvements to |Finesse| on all fronts.


.. only:: html

  .. cssclass:: avatar
  .. figure:: /images/sam.jpg

.. cssclass:: avatar-text
**Samuel Rowlinson**, University of Birmingham: developer with key contributions to the
beam tracing, higher-order modes features, integration with Cython, the code structure
and design, and the Sphinx documentation.


.. only:: html

  .. cssclass:: avatar
  .. figure:: /images/philip.jpg

.. cssclass:: avatar-text
**Philip Jones**, University of Birmingham: developer with key contributions to
the quantum noise implementation, signal and noise features, the legacy parser, and the
external Jupyter and Pygments extensions.


.. only:: html

  .. cssclass:: avatar
  .. figure:: /images/sean.jpg

.. cssclass:: avatar-text
**Sean Leavey**, Albert-Einstein-Institute Hannover: developer with key contributions to
the new KatScript syntax, parser and command line interface, test suite, continuous
integration tooling, the code structure and design, and the documentation.


Contributors to the Finesse development
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Contributors to the |Finesse| 3 code are listed below. Other contributions to earlier
versions are described in the :ref:`History <history>` section.

* **Aaron Jones**, University of Western Australia: developer with key contributions to
  the test suite, |Finesse| validation, documentation and continous integration. Primary
  developer of the BrumSoftTest validation tool, which was developed for |Finesse| validation.
* **Mischa Sallé**, Nikhef
* **Jan Just Keijser**, Nikhef
* **Duncan Macleod**, Cardiff University: Conda packaging, Windows fixes
* **Alexei Ciobanu**, University of Adelaide: faster map integration code
* **Lee McCuller**, ...
* **Huy Tuong Cao**, University of Adelaide: testing of thermal effects and FEA comparisons
* **Paul Hapke**, Albert-Einstein-Institute Hannover
* **Kevin Kuns**, MIT, LIGO and Cosmic Explorer: Contributions on new tests, bug fixes,
  and harrassing project lead and programming lead for new features and user interface
  improvements.

Contributions to testing, documenting and training
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Anna Green, University of Florida: LIGO training session, syntax reference and
  cheatsheet, |Finesse| workshops and ...
* Sean Leavey, AEI: |Finesse| workshop, |Finesse| manual, Ifosim logbook and ...
* Daniel Töyrä ...
* Charlotte Bond: |Finesse| notes and papers
* Daniel Brown and Andreas Freise: original |Finesse| manual and related papers,
  Finesse workshops and ...

That |Finesse| has remained as one of the main software packages in the field over many
years is to a large extend due to the efforts put into providing users with support and
training material, which is available via the main |Finesse| page
`<http://www.gwoptics.org/finesse/>`_.

Much of the physics behind the |Finesse| code has been compiled in the open access
Living Reviews article `Interferometer techniques for gravitational-wave detection
<https://link.springer.com/article/10.1007/s41114-016-0002-8>`_. We have described many
of the ideas that led to the current design of |Finesse| 3 in the the article `Pykat:
Python package for modelling precision optical interferometers
<https://www.sciencedirect.com/science/article/pii/S2352711020303265>`_.

We host a `logbook` sharing modelling work by the community
`<https://logbooks.ifosim.org/>`_. The logbook is powered by a `WordPress plugin
<https://alp.attackllama.com/>`_ by Sean Leavey and is maintained by Mischa Salle.
Logbooks are a common tool for recording progress in collaborative research projects, in
particular for large hardware projects such as gravitational wave detectors. Logbooks
record and preserve who did what when and thus over time create a searchable archive of
expert knowledge.

We also used the logbook to support one of our |Finesse| workshops
`<https://logbooks.ifosim.org/iucaa2019/>`_. You can see more examples of our in-person
teaching in workshops in our online tutorials on modelling laser interferometry
`<http://www.gwoptics.org/learn/>`_, composed by Daniel Töyrä.
